namespace ShoppingBasket
{
    public interface IVoucher
    {
        string Code { get; }
    }
}