using System.Collections.Generic;

namespace ShoppingBasket
{
    public interface IVoucherModiferRegistry
    {
        void Register<T>(IVoucherModifier modifier)
             where T : IVoucher;

        bool TryGetModifier(IVoucher voucher, out IVoucherModifier modifier);

        IEnumerable<IVoucherModifier> Modifiers { get; }
    }
}