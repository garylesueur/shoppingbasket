using System.Collections.Generic;

namespace ShoppingBasket
{
    public interface IVoucherModifier
    {
        IEnumerable<IVoucherResult> ApplyDiscounts(IBasket basket);
    }
}