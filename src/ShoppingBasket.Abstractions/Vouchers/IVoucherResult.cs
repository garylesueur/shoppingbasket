﻿namespace ShoppingBasket
{
    public interface IVoucherResult
    {
        string Message { get; }

        decimal Discount { get; }
    }
}