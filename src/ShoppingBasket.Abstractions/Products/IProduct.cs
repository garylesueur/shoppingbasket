﻿namespace ShoppingBasket
{
    public interface IProduct
    {
        string Category { get; }

        string Description { get; }

        decimal PricePerUnit { get; }
    }
}