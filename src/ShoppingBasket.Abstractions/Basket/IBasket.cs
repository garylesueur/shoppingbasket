using System.Collections.Generic;

namespace ShoppingBasket
{
    public interface IBasket
    {
        IEnumerable<IBasketItem> Items { get; }

        IEnumerable<IVoucher> Vouchers { get; }

        void Add(IVoucher voucher);

        void Add(IProduct product, int quantity);

        void SetQuantity(IProduct product, int quantity);

        void Remove(IProduct product);
    }
}