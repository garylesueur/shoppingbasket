﻿namespace ShoppingBasket
{
    public interface IBasketService
    {
        IBasketSummary GetSummary(IBasket basket);
    }
}