﻿using System.Collections.Generic;

namespace ShoppingBasket
{
    public interface IBasketSummary
    {
        IEnumerable<IVoucherResult> Discounts { get; }

        decimal Total { get; }

        decimal DiscountedTotal { get; }

        string Message { get; }
    }
}