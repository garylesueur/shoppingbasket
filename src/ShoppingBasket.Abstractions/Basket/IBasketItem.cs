namespace ShoppingBasket
{
    public interface IBasketItem
    {
        IProduct Product { get; }

        int Quantity { get; set; }
    }
}