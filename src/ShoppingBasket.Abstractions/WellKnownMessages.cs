namespace ShoppingBasket
{
    public static class WellKnownMessages
    {
        public static string MissedSpendThreshold(string code, decimal spendAnother, decimal discount)
        {
            return $"You have no reached the spend threshold for voucher {code}." +
                   $" Spend another {spendAnother:C} to receive {discount:C} discount from your basket total.";
        }

        public static string NoApplicableProducts(string code)
        {
            return $"There are no products in your basket applicable to voucher {code}";
        }

        public static string CantHaveMultipleOfferVouchers()
        {
            return "Only a single offer voucher can be applied to a basket";
        }
    }
}