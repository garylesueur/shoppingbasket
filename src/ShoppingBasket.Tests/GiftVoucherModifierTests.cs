using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;
using Rhino.Mocks;
using ShoppingBasket.Testing;

namespace ShoppingBasket.Tests
{
    public class GiftVoucherModifierTests : TestBase
    {
        private GiftVoucherModifier sut;

        private IBasket basket;
        private List<IBasketItem> basketItems;

        public override void TestSetup()
        {
            this.basketItems = new List<IBasketItem>();

            this.basket = this.GenerateMock<IBasket>();
            this.basket.Stub(x => x.Items).Return(this.basketItems);

            sut = new GiftVoucherModifier();
        }

        [Test]
        public void ReturnsZeroIfBasketIsEmpty()
        {
            // Arrange
            var voucher = new GiftVoucher("Code", 10);

            // Act
            var result = sut.ApplyDiscount(voucher, basket);

            // Assert
            result.Discount.Should().Be(0);
        }

        [Test]
        public void DoesNotReturnMoreThanBasketValue()
        {
            // Arrange
            basketItems.Add(TestHelpers.CreateBasketItem(3, 1));

            var voucher = new GiftVoucher("Code", 10);

            // Act
            var result = sut.ApplyDiscount(voucher, basket);

            // Assert
            result.Discount.Should().Be(3);
        }

        [Test]
        public void DoesNotIncludeGiftVouchersInDiscount()
        {
            // Arrange
            basketItems.Add(TestHelpers.CreateBasketItem(30, 1, WellKnownCategories.GiftVoucher));

            var voucher = new GiftVoucher("Code", 10);

            // Act
            var result = sut.ApplyDiscount(voucher, basket);

            // Assert
            result.Discount.Should().Be(0);
        }
    }
}