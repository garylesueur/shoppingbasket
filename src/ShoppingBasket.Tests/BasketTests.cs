﻿using System;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using ShoppingBasket.Testing;

namespace ShoppingBasket.Tests
{
    public class BasketTests : TestBase
    {
        private Basket sut;

        public override void TestSetup()
        {
            sut = new Basket();
        }

        [Test]
        public void CanAddAProductToABasket()
        {
            // Arrange
            var product = this.GenerateMock<IProduct>();

            // Act
            this.sut.Add(product);

            // Assert
            var bi = this.sut.Items.Single();
            bi.Product.Should().Be(product);
            bi.Quantity.Should().Be(1);
        }

        [Test]
        [TestCase(-1)]
        [TestCase(0)]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CantSpecifyANegativeQuantity(int quantity)
        {
            // Arrange
            var product = this.GenerateMock<IProduct>();

            // Act
            this.sut.Add(product, quantity);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CantSpecifyNullProduct()
        {
            this.sut.Add(null, 1);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CantSpecifyNullVoucher()
        {
            this.sut.Add((IVoucher)null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MustSpecifyProductToRemove()
        {
            // Act
            sut.Remove((IProduct)null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MustSpecifyVoucherToRemove()
        {
            // Act
            sut.Remove((IVoucher)null);
        }

        [Test]
        public void CanAddAProductAndSpecifyTheQuantity()
        {
            // Arrange
            var product = this.GenerateMock<IProduct>();
            var quantity = 2;

            // Act
            this.sut.Add(product, quantity);

            // Assert
            var bi = this.sut.Items.Single();
            bi.Product.Should().Be(product);
            bi.Quantity.Should().Be(quantity);
        }

        [Test]
        public void WhenAddingIfTheProductAlreadyExistsThenQuantityIsIncreased()
        {
            // Arrange
            var product = this.GenerateMock<IProduct>();
            this.sut.Add(product, 10);

            // Act
            this.sut.Add(product, 20);

            // Assert
            var bi = this.sut.Items.Single();
            bi.Quantity.Should().Be(30);
        }

        [Test]
        public void CanRemoveProductFromBasket()
        {
            // Arrange
            var product = this.GenerateMock<IProduct>();
            this.sut.Add(product, 10);

            // Act
            this.sut.Remove(product);

            // Assert
            this.sut.Items.Should().BeEmpty();
        }

        [Test]
        public void CanSetTheProductQuantity()
        {
            var product = this.GenerateMock<IProduct>();
            this.sut.Add(product);
            var quantity = 10;

            // Act
            this.sut.SetQuantity(product, quantity);

            // Assert
            this.sut.Items.Single().Quantity.Should().Be(quantity);
        }

        [Test]
        public void IfProductDoesntExistWhenSettingQuantityThenItIsAdded()
        {
            // Arrange
            var product = this.GenerateMock<IProduct>();
            var quantity = 10;

            // Act
            this.sut.SetQuantity(product, quantity);

            // Assert
            var bi = this.sut.Items.Single();
            bi.Product.Should().Be(product);
            bi.Quantity.Should().Be(quantity);
        }

        [Test]
        public void CanAddVoucherToABasket()
        {
            // Arrange
            var voucher = this.GenerateMock<IVoucher>();

            // Act
            sut.Add(voucher);

            // Assert
            sut.Vouchers.Single().Should().Be(voucher);
        }

        [Test]
        public void CanRemoveVoucherFromBasket()
        {
            // Arrange
            var voucher = this.GenerateMock<IVoucher>();
            sut.Add(voucher);

            // Act
            sut.Remove(voucher);

            // Assert
            sut.Vouchers.Should().BeEmpty();
        }
    }
}
