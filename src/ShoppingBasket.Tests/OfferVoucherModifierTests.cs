using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using Rhino.Mocks;
using ShoppingBasket.Testing;

namespace ShoppingBasket.Tests
{
    public class OfferVoucherModifierTests : TestBase
    {
        private OfferVoucherModifier  sut;

        private IBasket basket;
        private List<IBasketItem> basketItems;
        private List<IVoucher> vouchers;

        public override void TestSetup()
        {
            this.basketItems = new List<IBasketItem>();
            this.vouchers = new List<IVoucher>();

            this.basket = this.GenerateMock<IBasket>();
            this.basket.Stub(x => x.Items).Return(this.basketItems);
            this.basket.Stub(x => x.Vouchers).Return(this.vouchers);

            sut = new OfferVoucherModifier();
        }

        [Test]
        public void DoesNotAllowMultipleOfferVouchers()
        {
            // Arrange
            basketItems.Add(TestHelpers.CreateBasketItem(30, 1));

            var voucher1 = new OfferVoucher("Code", 10, 0, string.Empty);
            var voucher2 = new OfferVoucher("Code", 20, 0, string.Empty);

            vouchers.Add(voucher1);
            vouchers.Add(voucher2);

            // Act
            var results = sut.ApplyDiscounts(basket);

            // Assert
            var result = results.Single();
            result.Discount.Should().Be(0);
            result.Message.Should().Be(WellKnownMessages.CantHaveMultipleOfferVouchers());
        }

        [Test]
        public void ReturnsZeroIfBasketIsEmpty()
        {
            // Arrange
            var voucher = new OfferVoucher("Code", 10, 50, string.Empty);

            // Act
            var result = sut.ApplyDiscount(voucher, basket);

            // Assert
            result.Discount.Should().Be(0);
        }

        [Test]
        public void DoesNotReturnMoreThanBasketValue()
        {
            // Arrange
            basketItems.Add(TestHelpers.CreateBasketItem(5, 1));

            var voucher = new OfferVoucher("Code", 10, 0, string.Empty);

            // Act
            var result = sut.ApplyDiscount(voucher, basket);

            // Assert
            result.Discount.Should().Be(5);
        }

        [Test]
        public void DoesNotApplyDiscountIfHasntMetThreshold()
        {
            // Arrange
            basketItems.Add(TestHelpers.CreateBasketItem(5, 1));

            var voucher = new OfferVoucher("Code", 10, 10, string.Empty);

            // Act
            var result = sut.ApplyDiscount(voucher, basket);

            // Assert
            result.Discount.Should().Be(0);
        }

        [Test]
        public void OnlyApplysDiscountToMatchingCategoryItems()
        {
            // Arrange
            basketItems.Add(TestHelpers.CreateBasketItem(20, 1, "None"));
            basketItems.Add(TestHelpers.CreateBasketItem(5, 1, "TestCat"));

            var voucher = new OfferVoucher("Code", 10, 0, "TestCat");

            // Act
            var result = sut.ApplyDiscount(voucher, basket);

            // Assert
            result.Discount.Should().Be(5);
        }
    }
}