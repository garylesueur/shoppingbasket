using System.Diagnostics;
using FluentAssertions;
using NUnit.Framework;
using ObjectDumper;
using ShoppingBasket.Testing;

namespace ShoppingBasket.Tests
{
    public class BusinessIntegrationTests : TestBase
    {
        private IBasket basket;
        private IBasketService basketService;

        public override void TestSetup()
        {
            this.basket = new Basket();

            var registry = new VoucherModiferRegistry();
            registry.Register<OfferVoucher>(new OfferVoucherModifier());
            registry.Register<GiftVoucher>(new GiftVoucherModifier());

            this.basketService = new BasketService(registry);
        }

        [Test]
        public void Basket1()
        {
            // Arrange
            var hat = new Product("Hat", 10.50M);
            var jumper = new Product("Jumper", 54.65M);
            var giftVoucher = new GiftVoucher("XXX-XXX", 5.0M);

            basket.Add(hat, 1);
            basket.Add(jumper, 1);
            basket.Add(giftVoucher);

            // Act
            var result = basketService.GetSummary(basket);

            // Assert
            result.DiscountedTotal.Should().Be(60.15M);
            Trace.WriteLine(result.DumpToString("Result"));
        }

        [Test]
        public void Basket2()
        {
            // Arrange
            var hat = new Product("Hat", 25M);
            var jumper = new Product("Jumper", 26M);
            var offerVoucher = new OfferVoucher("YYY-YYY", 5.0M, 50M, "HeadGear");

            basket.Add(hat, 1);
            basket.Add(jumper, 1);
            basket.Add(offerVoucher);

            // Act
            var result = basketService.GetSummary(basket);

            // Assert
            result.DiscountedTotal.Should().Be(51M);
            result.Message.Should().Be(WellKnownMessages.NoApplicableProducts("YYY-YYY"));
        }

        [Test]
        public void Basket3()
        {
            // Arrange
            var hat = new Product("Hat", 25M);
            var jumper = new Product("Jumper", 26M);
            var headlight = new Product("Head Light", 3.50M, "HeadGear");
            var offerVoucher = new OfferVoucher("YYY-YYY", 5.0M, 50M, "HeadGear");

            basket.Add(hat, 1);
            basket.Add(jumper, 1);
            basket.Add(headlight, 1);
            basket.Add(offerVoucher);

            // Act
            var result = basketService.GetSummary(basket);

            // Assert
            result.DiscountedTotal.Should().Be(51M);
            result.Message.Should().BeNullOrEmpty();
        }

        [Test]
        public void Basket4()
        {
            // Arrange
            var hat = new Product("Hat", 25M);
            var jumper = new Product("Jumper", 26M);
            var giftVoucher = new GiftVoucher("XXX-XXX", 5.0M);
            var offerVoucher = new OfferVoucher("YYY-YYY", 5.0M, 50M, string.Empty);

            basket.Add(hat, 1);
            basket.Add(jumper, 1);
            basket.Add(giftVoucher);
            basket.Add(offerVoucher);

            // Act
            var result = basketService.GetSummary(basket);

            // Assert
            result.DiscountedTotal.Should().Be(41M);
            result.Message.Should().BeNullOrEmpty();
        }

        [Test]
        public void Basket5()
        {
            // Arrange
            var hat = new Product("Hat", 25M);
            var giftVoucher = new Product("�30 Gift Voucher", 30M, WellKnownCategories.GiftVoucher);

            var offerVoucher = new OfferVoucher("YYY-YYY", 5.0M, 50M, string.Empty);

            basket.Add(hat, 1);
            basket.Add(giftVoucher, 1);

            basket.Add(offerVoucher);

            // Act
            var result = basketService.GetSummary(basket);

            // Assert
            result.DiscountedTotal.Should().Be(55M);
            result.Message.Should().Be(WellKnownMessages.MissedSpendThreshold("YYY-YYY", 25.01M, 5M));
        }
    }
}