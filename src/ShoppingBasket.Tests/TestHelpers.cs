﻿using Rhino.Mocks;

namespace ShoppingBasket.Tests
{
    public static class TestHelpers
    {
        public static IProduct CreateProduct(decimal price)
        {
            var product = MockRepository.GenerateMock<IProduct>();
            product.Stub(p => p.PricePerUnit).Return(price);
            return product;
        }

        public static IBasketItem CreateBasketItem(decimal price)
        {
            return CreateBasketItem(price, 1);
        }

        public static IBasketItem CreateBasketItem(decimal price, int quantity, string category = null)
        {
            var product = MockRepository.GenerateMock<IProduct>();
            product.Stub(p => p.PricePerUnit).Return(price);
            product.Stub(p => p.Category).Return(category);

            var item = MockRepository.GenerateMock<IBasketItem>();
            item.Stub(i => i.Product).Return(product);
            item.Stub(i => i.Quantity).Return(quantity);
            return item;
        }
    }
}