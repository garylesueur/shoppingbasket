﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using Rhino.Mocks;
using ShoppingBasket.Testing;

namespace ShoppingBasket.Tests
{
    public class BasketServiceTests : TestBase
    {
        private BasketService sut;

        private IBasket basket;
        private List<IBasketItem> basketItems;

        public override void TestSetup()
        {
            this.basketItems = new List<IBasketItem>();
            this.basket = this.GenerateMock<IBasket>();
            this.basket.Stub(x => x.Items).Return(this.basketItems);
            this.basket.Stub(x => x.Vouchers).Return(Enumerable.Empty<IVoucher>());

            this.sut = new BasketService(new VoucherModiferRegistry());
        }

        [Test]
        public void TotalisZeroIfNoItemsInBasket()
        {
            // Act
            var summary = this.sut.GetSummary(basket);

            // Assert
            summary.Total.Should().Be(0);
        }

        [Test]
        public void TotalPriceWillMatchProductPriceWhenSingleItemInBasket()
        {
            // Arrange
            var price = this.GetRandomDecimal();
            this.basketItems.Add(TestHelpers.CreateBasketItem(price, 1));

            // Act
            var summary = this.sut.GetSummary(basket);

            // Assert
            summary.Total.Should().Be(price);
        }

        [Test]
        public void TotalPriceWillMatchProductPriceMultipledByQuantityWhenSingleItemInBasket()
        {
            // Arrange
            var price = this.GetRandomDecimal();
            var quantity = this.GetRandomInt32(10, 20);
            this.basketItems.Add(TestHelpers.CreateBasketItem(price, quantity));

            // Act
            var summary = this.sut.GetSummary(basket);

            // Assert
            summary.Total.Should().Be(price * quantity);
        }

        [Test]
        public void TotalPriceWillMatchSumOfAllProductsPricesInBasket()
        {
            // Arrange
            this.basketItems.Add(TestHelpers.CreateBasketItem(1));
            this.basketItems.Add(TestHelpers.CreateBasketItem(2));
            this.basketItems.Add(TestHelpers.CreateBasketItem(3));
            this.basketItems.Add(TestHelpers.CreateBasketItem(4));

            // Act
            var summary = this.sut.GetSummary(basket);

            // Assert
            summary.Total.Should().Be(1 + 2 + 3 + 4);
        }

        [Test]
        public void TotalPriceWillMatchSumOfAllProductsPricesByQuantityInBasket()
        {
            // Arrange
            var quantity = this.GetRandomInt32(10, 20);
            this.basketItems.Add(TestHelpers.CreateBasketItem(1, quantity));
            this.basketItems.Add(TestHelpers.CreateBasketItem(2, quantity));
            this.basketItems.Add(TestHelpers.CreateBasketItem(3, quantity));
            this.basketItems.Add(TestHelpers.CreateBasketItem(4, quantity));

            // Act
            var summary = this.sut.GetSummary(basket);

            // Assert
            summary.Total.Should().Be((1 + 2 + 3 + 4) * quantity);
        }
    }
}