﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ShoppingBasket
{
    public class Basket : IBasket
    {
        private readonly List<IBasketItem> items;
        private readonly List<IVoucher> vouchers;

        public Basket()
        {
            this.items = new List<IBasketItem>();
            this.vouchers = new List<IVoucher>();
        }

        public IEnumerable<IBasketItem> Items => this.items;

        public IEnumerable<IVoucher> Vouchers => this.vouchers;

        public void Add(IProduct product)
        {
            this.Add(product, 1);
        }

        public void Add(IProduct product, int quantity)
        {
            if (quantity <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(quantity), "must be greater than zero");
            }

            if (product == null)
            {
                throw new ArgumentNullException(nameof(product));
            }

            var existingItem = this.items.SingleOrDefault(i => i.Product == product);

            if (existingItem != null)
            {
                existingItem.Quantity += quantity;
                return;
            }

            this.items.Add(new BasketItem()
            {
                Product = product,
                Quantity = quantity,
            });
        }

        public void SetQuantity(IProduct product, int quantity)
        {
            var basketItem = this.items.SingleOrDefault(i => i.Product == product);

            if (basketItem == null)
            {
                this.Add(product, quantity);
                return;
            }

            basketItem.Quantity = quantity;
        }

        public void Remove(IProduct product)
        {
            if (product == null)
            {
                throw new ArgumentNullException(nameof(product));
            }

            this.items.RemoveAll(i => i.Product == product);
        }

        public void Add(IVoucher voucher)
        {
            if (voucher == null)
            {
                throw new ArgumentNullException(nameof(voucher));
            }

            this.vouchers.Add(voucher);
        }

        public void Remove(IVoucher voucher)
        {
            if (voucher == null)
            {
                throw new ArgumentNullException(nameof(voucher));
            }

            this.vouchers.Remove(voucher);
        }
    }
}