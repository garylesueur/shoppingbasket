﻿namespace ShoppingBasket
{
    public class BasketItem : IBasketItem
    {
        public IProduct Product { get; set; }

        public int Quantity { get; set; }
    }
}