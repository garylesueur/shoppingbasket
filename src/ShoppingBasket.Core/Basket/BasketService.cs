﻿using System.Collections.Generic;
using System.Linq;

namespace ShoppingBasket
{
    public class BasketService : IBasketService
    {
        private readonly IVoucherModiferRegistry voucherModiferRegistry;

        public BasketService(IVoucherModiferRegistry voucherModiferRegistry)
        {
            this.voucherModiferRegistry = voucherModiferRegistry;
        }

        public IBasketSummary GetSummary(IBasket basket)
        {
            var total = basket.Items.Sum(i => i.Product.PricePerUnit * i.Quantity);

            var voucherResults = new List<IVoucherResult>();

            foreach (var modifier in voucherModiferRegistry.Modifiers)
            {
                voucherResults.AddRange(
                    modifier.ApplyDiscounts(basket));
            }

            var totalDiscount = voucherResults.Sum(r => r.Discount);

            var messages = string.Concat(voucherResults.Select(x => x.Message));

            return new BasketSummary()
            {
                Discounts = voucherResults,
                Message = messages,
                Total = total,
                DiscountedTotal = total - totalDiscount,
            };
        }
    }
}