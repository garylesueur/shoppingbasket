using System.Collections.Generic;

namespace ShoppingBasket
{
    public class BasketSummary : IBasketSummary
    {
        public IEnumerable<IVoucherResult> Discounts { get; set; }

        public decimal Total { get; set; }

        public decimal DiscountedTotal { get; set; }

        public string Message { get; set; }
    }
}