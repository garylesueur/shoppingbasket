namespace ShoppingBasket
{
    public class Product : IProduct
    {
        public Product(string description, decimal price)
            :this(description, price, null)
        {
        }

        public Product(string description, decimal price, string category)
        {
            this.Description = description;
            this.PricePerUnit = price;
            this.Category = category;
        }

        public string Description { get; }

        public decimal PricePerUnit { get; }

        public string Category { get; }
    }
}