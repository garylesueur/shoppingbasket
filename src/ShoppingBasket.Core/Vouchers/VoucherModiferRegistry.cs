using System;
using System.Collections.Generic;

namespace ShoppingBasket
{
    public class VoucherModiferRegistry : IVoucherModiferRegistry
    {
        private readonly Dictionary<Type, IVoucherModifier> registry;

        public VoucherModiferRegistry()
        {
            this.registry = new Dictionary<Type, IVoucherModifier>();
        }

        public IEnumerable<IVoucherModifier> Modifiers => registry.Values;

        public void Register<T>(IVoucherModifier modifier)
            where T : IVoucher
        {
            this.registry[typeof (T)] = modifier;
        }

        public bool TryGetModifier(IVoucher voucher, out IVoucherModifier modifier)
        {
            return registry.TryGetValue(voucher.GetType(), out modifier);
        }
    }
}