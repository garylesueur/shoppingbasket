﻿using NUnit.Framework;

namespace ShoppingBasket.Testing
{
    [TestFixture]
    public abstract class TestBase
    {
        [TestFixtureSetUp]
        public virtual void FixtureSetup()
        {
        }

        [TestFixtureTearDown]
        public virtual void FixtureCleanUp()
        {
        }

        /// <summary>
        /// Sets up each individual test before execution.
        /// </summary>
        [SetUp]
        public void TestSetupCore()
        {
            this.TestSetup();
        }

        public virtual void TestSetup()
        {
        }

        [TearDown]
        public virtual void TestCleanUp()
        {
        }
    }
}
