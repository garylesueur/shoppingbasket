﻿using System;
using System.Collections.Generic;
using Rhino.Mocks;

namespace ShoppingBasket.Testing
{
    // These are a bunch of helper functions I copied from my Toolbox
    public static class TestExtensions
    {
        public static void GenerateMock<T>(this TestBase b, out T mock)
            where T : class
        {
            mock = MockRepository.GenerateMock<T>();
        }

        public static T GenerateMock<T>(this TestBase b)
            where T : class
        {
            return MockRepository.GenerateMock<T>();
        }

        public static T[] GenerateMocks<T>(this TestBase b, Action<T> setup)
            where T : class
        {
            var result = new List<T>();
            var count = b.GetRandomInt32(3, 10);
            for (int i = 0; i < count; i++)
            {
                var mock = MockRepository.GenerateMock<T>();
                setup?.Invoke(mock);

                result.Add(mock);
            }

            return result.ToArray();
        }

        public static T[] GenerateMocks<T>(this TestBase b) where T : class
        {
            return b.GenerateMocks<T>(null);
        }

        public static T[] GenerateItems<T>(this TestBase b) where T : class, new()
        {
            return b.GenerateItems<T>(null);
        }

        public static T[] GenerateItems<T>(this TestBase b, Action<T> setup) where T : class, new()
        {
            var result = new List<T>();
            var count = b.GetRandomInt32(3, 10);
            for (int i = 0; i < count; i++)
            {
                var mock = new T();
                setup?.Invoke(mock);

                result.Add(mock);
            }

            return result.ToArray();
        }

        public static T1 GenerateMock<T1, T2>(this TestBase b)
            where T1 : class
        {
            return (T1)MockRepository.GenerateMock(typeof(T1), new[] { typeof(T2) });
        }
    }
}
