using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShoppingBasket.Testing
{    
    // These are a bunch of helper functions I copied from my Toolbox

    public static class RandomTestExtensions
    {
        private static readonly Lazy<Random> rnd;

        static RandomTestExtensions()
        {
            rnd = new Lazy<Random>(() => new Random());
        }

        public static Random Rnd => rnd.Value;

        public static byte[] GetRandomBytes(this TestBase b)
        {
            return b.GetRandomBytes(b.GetRandomInt32(1, 20));
        }

        public static byte[] GetRandomBytes(this TestBase b, int size)
        {
            var result = new byte[size];
            Rnd.NextBytes(result);
            return result;
        }

        /// <summary>
        /// A string containing random letters only.
        /// </summary>
        public static string GetRandomText(this TestBase b)
        {
            var sb = new StringBuilder();
            var len = b.GetRandomInt32(5, 20);
            for (int i = 0; i < len; i++)
            {
                sb.Append((char)b.GetRandomInt32(65, 90));
            }

            return sb.ToString();
        }


        public static T[] GetRandomArray<T>(this TestBase b, Func<int, T> factory)
        {
            var len = b.GetRandomInt32(5, 20);
            var result = new T[len];
            for (int i = 0; i < len; i++)
            {
                result[i] = factory(i);
            }
            return result;
        }

        /// <summary>
        ///  A string containing Random characters including numbers and symbols.
        /// </summary>
        public static string GetRandomString(this TestBase b)
        {
            var sb = new StringBuilder();
            var len = b.GetRandomInt32(5, 20);
            for (int i = 0; i < len; i++)
            {
                sb.Append((char)b.GetRandomInt32(32, 126));
            }

            return sb.ToString();
        }

        public static Guid GetRandomGuid(this TestBase b)
        {
            return Guid.NewGuid();
        }

        public static T GetRandomEnum<T>(this TestBase b)
        {
            var list = Enum.GetValues(typeof(T)).Cast<T>().ToList();

            var index = b.GetRandomInt32(0, list.Count);

            return list[index];
        }

        public static TimeSpan GetRandomTime(this TestBase b)
        {
            return TimeSpan.FromSeconds(
                b.GetRandomInt32(0, (int)TimeSpan.FromDays(30).TotalSeconds));
        }

        public static DateTime GetRandomDate(this TestBase b)
        {
            return DateTime.UtcNow.AddDays(b.GetRandomInt32(-100, 100));
        }

        public static double GetRandomDouble(this TestBase b)
        {
            return Rnd.NextDouble();
        }

        public static decimal GetRandomDecimal(this TestBase b)
        {
            return (decimal)Rnd.NextDouble();
        }

        public static bool GetRandomBool(this TestBase b)
        {
            return b.GetRandomInt32(-100, 100) > 0;
        }

        public static Int32 GetRandomInt32(this TestBase b)
        {
            var buf = new byte[4];
            Rnd.NextBytes(buf);
            return BitConverter.ToInt32(buf, 0);
        }

        public static Int32 GetRandomInt32(this TestBase b, int minvalue)
        {
            return b.GetRandomInt32(minvalue, Int32.MaxValue);
        }

        public static Int32 GetRandomInt32(this TestBase b, int minvalue, int maxvalue)
        {
            return Rnd.Next(minvalue, maxvalue);
        }

        public static List<T> GetRandomList<T>(this TestBase b, int min, int max, Func<int, T> factory)
        {
            var count = b.GetRandomInt32(min, max);
            var result = new List<T>(count);

            for (int i = 0; i < count; i++)
            {
                result.Add(factory(i));
            }

            return result;
        }
    }
}