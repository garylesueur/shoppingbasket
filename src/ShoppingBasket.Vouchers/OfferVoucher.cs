namespace ShoppingBasket
{
    public class OfferVoucher : IVoucher
    {
        public OfferVoucher(string code, decimal discount, decimal threshold, string category)
        {
            this.Code = code;
            this.Discount = discount;
            this.Threshold = threshold;
            this.Category = category;
        }

        public string Code { get; }

        public decimal Discount { get; }

        public decimal Threshold { get; }

        public string Category { get; }
    }
}