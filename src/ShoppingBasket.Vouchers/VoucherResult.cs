﻿namespace ShoppingBasket
{
    public class VoucherResult : IVoucherResult
    {
        public static readonly VoucherResult Empty = new VoucherResult()
        {
            Discount = 0,
            Message = string.Empty,
        };

        public decimal Discount { get; set; }

        public string Message { get; set; }
    }
}