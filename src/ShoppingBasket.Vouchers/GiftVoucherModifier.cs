using System;
using System.Collections.Generic;
using System.Linq;

namespace ShoppingBasket
{
    public class GiftVoucherModifier : IVoucherModifier
    {
        public IEnumerable<IVoucherResult> ApplyDiscounts(IBasket basket)
        {
            return basket.Vouchers.OfType<GiftVoucher>().Select(voucher => ApplyDiscount(voucher, basket));
        }

        public IVoucherResult ApplyDiscount(IVoucher voucher, IBasket basket)
        {
            var gv = (GiftVoucher) voucher;

            if (!basket.Items.Any())
            {
                return VoucherResult.Empty;
            }

            var discountableTotal = basket.Items
                .Where(i => i.Product.Category != WellKnownCategories.GiftVoucher)
                .Sum(i => i.Product.PricePerUnit * i.Quantity);
            
            return new VoucherResult()
            {
                Discount = Math.Min(discountableTotal, gv.Value),
            };
        }
    }
}