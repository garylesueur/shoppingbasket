using System;
using System.Collections.Generic;
using System.Linq;

namespace ShoppingBasket
{
    public class OfferVoucherModifier : IVoucherModifier
    {
        public IEnumerable<IVoucherResult> ApplyDiscounts(IBasket basket)
        {
            var vouchers = basket.Vouchers
                .OfType<OfferVoucher>().ToArray();

            if (vouchers.Length > 1)
            {
                // Error!
                return new[] {new VoucherResult()
                {
                    Discount = 0,
                    Message = WellKnownMessages.CantHaveMultipleOfferVouchers()
                },};
            }

            return vouchers
                .Select(voucher => ApplyDiscount(voucher, basket));
        }

        public IVoucherResult ApplyDiscount(IVoucher voucher, IBasket basket)
        {
            var ov = (OfferVoucher)voucher;

            var items = basket.Items
                .Where(i => i.Product.Category != WellKnownCategories.GiftVoucher);

            var discountableTotal = items
                .Sum(i => i.Product.PricePerUnit * i.Quantity);

            if (discountableTotal < ov.Threshold)
            {
                return new VoucherResult()
                {
                    Message = WellKnownMessages.MissedSpendThreshold(ov.Code, ov.Threshold - discountableTotal + 0.01M, ov.Discount)
                };
            }

            if (!string.IsNullOrEmpty(ov.Category))
            {
                return SpecificCategoryResult(voucher, basket, ov, items);
            }

            return new VoucherResult()
            {
                Discount = Math.Min(discountableTotal, ov.Discount),
            };
        }

        private static IVoucherResult SpecificCategoryResult(
            IVoucher voucher, IBasket basket, OfferVoucher ov, IEnumerable<IBasketItem> items)
        {
            if (basket.Items.All(i => i.Product.Category != ov.Category))
            {
                return new VoucherResult()
                {
                    Message = WellKnownMessages.NoApplicableProducts(voucher.Code)
                };
            }
            else
            {
                var maxDiscount = items
                    .Where(i => i.Product.Category == ov.Category)
                    .Sum(i => i.Product.PricePerUnit * i.Quantity);

                return new VoucherResult()
                {
                    Discount = Math.Min(maxDiscount, ov.Discount),
                };
            }
        }
    }
}