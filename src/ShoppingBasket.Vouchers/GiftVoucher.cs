﻿namespace ShoppingBasket
{
    public class GiftVoucher : IVoucher
    {
        public GiftVoucher(string code, decimal value)
        {
            this.Code = code;
            this.Value = value;
        }

        public decimal Value { get; }

        public string Code { get; }
    }
}